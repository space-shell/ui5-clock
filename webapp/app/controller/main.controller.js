sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/w/clock/scr/core",
	"sap/ui/w/clock/scr/sha1",
	"sap/ui/model/json/JSONModel",
	"sap/ui/w/clock/scr/xmlToJSON",
	"sap/ui/w/clock/app/models/formatter"
], function (Controller, coreHash, sha1Hash, JSONModel, xml, formatter) {
	"use strict";

	return Controller.extend("sap.ui.w.clock.app.controller.main", {

		formatter: formatter,

		onInit: function() {
			sap.ui.getCore().User = localStorage.getItem("User");
			this.userCheck(sap.ui.getCore().User);

			//this.baseGet("time_entries/report.xml?subject_id=12393892"); //Enable CORS on the Basecamp Server!!!
			
		},

		userCheck: function(){
			var localModel = this.getOwnerComponent().getModel("remoteData");
			localModel.attachRequestCompleted(() => {
				let oModel = new sap.ui.model.json.JSONModel(localModel.getProperty("/Data/" + localStorage.getItem("User")));
				this.getOwnerComponent().setModel(oModel, "User");
			});
		},


		baseGet: function(query){
			return fetch("https://weaveability.basecamphq.com/" + query, {
				method: 'GET',
				mode: 'no-cors',
				headers: {
					'Accept': 'application/xml',
					'Authorization': 'Basic ' + btoa('7ce2aec784de85d6ef3a49a31e6f8629e423abb3:X'),
					'Content-Type': 'application/xml',
					'User-Agent': 'Clock'
				}
			})
			.then((data) => {
				console.log(data);
				let xml = xmlToJSON.parseString(data);
				return JSON.stringify(xml);
			}).catch((err) => {
				console.log(err);
			});
		},

		onAfterRendering: function() {

		},

		clock: function(id){
			fetch("https://1ff9r7jem2.execute-api.eu-west-1.amazonaws.com/prod/clock?key=" + id.toString(), {
				method: 'GET',
				credentials: "include",
				headers: {
					'Accept': 'application/json'
				}
			})
			.then((data) => data.json())
			.then((usrData) => {
				
				localStorage.setItem("User", usrData.Data);
				sap.ui.getCore().User = localStorage.getItem("User");
			}).catch((err) => {
				console.log(err);
			});
		},

		pinLiveChange: function(oEvt) {
			let keyVal = oEvt.getParameter("value");
			if(keyVal.length === 4){
				this.clock(CryptoJS.SHA1(keyVal));
				oEvt.getSource().setValue("");
				//window.location.reload(true);
			}
		}

	});
});