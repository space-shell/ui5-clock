sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/w/clock/app/models/formatter"
], function (Controller, JSONModel, formatter) {
	"use strict";

	return Controller.extend("sap.ui.w.clock.app.view.block.controller.homeBlockController", {

		formatter: formatter,

		onInit: function (oEvent) {

		}
	});
});